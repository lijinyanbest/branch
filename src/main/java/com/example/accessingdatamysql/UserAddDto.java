package com.example.accessingdatamysql;


public class UserAddDto {

    private String name;

    private String email;
    
    private String password;

    public String getname(){
        return name;
    }

    public void setname(String name){
        this.name =  name;
    }

    public String getpassword(){
        return password;
    }

    public void setpassword(String password){
        this.password = password;
    }

    public String getemail(){
        return email;
    }

    public void setemail(String email){
        this.email = email;
    }
}